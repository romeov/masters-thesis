((org-mode . ((eval . (require 'ox-latex))
              (eval . (setq org-latex-pdf-process '("latexmk --shell-escape --synctex=1 -f -%latex -interaction=nonstopmode -output-directory=%o %f")))
              (eval . (setq  org-latex-reference-command "\\cref{%s}"))
              (eval . (setq org-latex-title-command ""))
              (eval . (setq org-latex-prefer-user-labels t))
              ;; (eval . (setq  org-export-with-broken-links t))
              (eval . (setq org-latex-src-block-backend 'engraved))
              (eval . (setq  org-latex-engraved-preamble (replace-regexp-in-string "breakable,?" "" org-latex-engraved-preamble)))
              (eval . (setcdr (assoc "breaklines" org-latex-engraved-options) "false"))
              (eval . (conda-env-activate "julia")))))

;; (eval . (setq  org-latex-listings 'engraved))
;; (eval . (setq  org-latex-engraved-preamble (replace-regexp-in-string "breakable,?" "" org-latex-engraved-preamble)))
;; (eval . (setcdr (assoc "breaklines" org-latex-engraved-options) "false"))

;; (eval . (setq org-latex-minted-options '(("frame" "lines") ("bgcolor" "LightYellow"))))
;; For envgraved, remove ~breakable~ in ~\usepackage{tcolorbox}~ for =org-latex-engraved-preamble=.
;; e.g. using ~string-replace~
;; Also, in ~org-latex-engraved-options~ set ~("breaklines" . "false")~ and optionally remove the ~breaksymbol~.
